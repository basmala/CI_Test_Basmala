package at.spengergasse.spengermed;


import at.spengergasse.spengermed.models.*;
import at.spengergasse.spengermed.repositories.PractitionerRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class PractitionerRepositoryTest {
    @Autowired // erstellt automatisch Instanz
    // erstellt Practitioner-objekt
            PractitionerRepository practitionerRepository;

    @Test
    @Transactional // Alles oder nichts (alle DB-Befehle/ keine)
    public void testSaveAndLoadOnePractitioner() {
        //1. Erstellen einer mit Daten befüllten Practitioner-instanz
        Practitioner pr = returnOnePractitioner();

        //2. Instanz in die DB speichern
        Practitioner savedPr = practitionerRepository.save(pr);

        // 3. Gespeicherte Daten aus der DB lesen
        Practitioner loadedPractitioner = practitionerRepository.findById(savedPr.getId()).get();
        System.out.println(loadedPractitioner);

        // 4. Vergleich des gespeicherten Objekts mit dem geladenen (Attribute zwischen p & loadedp)

        // Alle einfachen Datentypen können mit Equals verglichen werden.
        // Assert prüft, ob die beiden gleich sind. Schlägt ein Assert fehl, ist der Test fehlgeschlagen/
        // /Asserts sind die eigentlichen "Tests"

        assertEquals(pr.getBirthDate(), loadedPractitioner.getBirthDate());
        assertEquals(pr.getGender(), loadedPractitioner.getGender());
        assertEquals(pr.getText(), loadedPractitioner.getText());
        //Es sollen alle Attribute verglichen werden, auch die geerbten.

        // Collections werden mit CollectionUtils auf Gleichheit getestet.
        // Dabei werden die einzelnen Elemente verglichen, nicht ob die Collectionobjekte gleich sind.
        assertTrue(CollectionUtils.isEqualCollection(pr.getIdentifier(), loadedPractitioner.getIdentifier()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getName(), loadedPractitioner.getName()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getTelecom(), loadedPractitioner.getTelecom()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getAddress(), loadedPractitioner.getAddress()));

        assertTrue(CollectionUtils.isEqualCollection(pr.getCommunication(), loadedPractitioner.getCommunication()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getPhoto(), loadedPractitioner.getPhoto()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getQualification(), loadedPractitioner.getQualification()));

        //Es sollen alle Collections getestet werden.
        }
        // Ein Practitionerobjekt. Alle Attribute sollen Werte bekommen.
    // Auch die Collections sollen zumindest 1 Wert haben.
    public static Practitioner returnOnePractitioner(){
        List<Identifier> identifiers = new ArrayList<>();
        List<Coding> codings = new ArrayList<>();
        List<ContactPoint> contactPoints = new ArrayList<>();
        List<HumanName> names = new ArrayList<>();
        List<Address> address = new ArrayList<>();
        List<String> prefixes = null;
        List<String> suffixes = null;
        List<Attachment> photos = new ArrayList<>();
        List<CodeableConcept> communications = new ArrayList<>();
        List<Qualification> qualifications = new ArrayList<>();
        List<Identifier> qidentifiers = new ArrayList<>();

        //Ein Coding Objekt wird mit wie bisher mit einem Konstruktor gebaut.
        codings.add(new Coding("System", "0.1.1", "Code", "<div>...<div>", false));

        //Eine Period mit Konstruktor
        Period period = new Period(LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2001,01,01,1,1));
        //Eine Period mit dem Builder Pattern. Es ist offensichtlicher, welche Attribute gesetzt werden.
        Period period2 = Period.builder()
                .start(LocalDateTime.of(2000, 01,01,1,1))
                .end(LocalDateTime.of(2010, 02,02,2,2))
                .build();
        Period period3 = Period.builder()
                .start(LocalDateTime.of(2001, 01,01,1,1))
                .end(LocalDateTime.of(2011, 02, 02, 2, 2))
                .build();

        CodeableConcept ccType = CodeableConcept.builder()
                .coding(codings)
                .text("<div></div>")
                .build(); // new CodeableConcept(codings, "Text");
        communications.add(ccType);


        identifiers.add(
                Identifier.builder()
                        .code(Identifier.UseCode.official)
                        .period(period)
                        .system("System")
                        .type(ccType)
                        .value("value")
                        .build()
        );
        qidentifiers.add(
                Identifier.builder()
                        .code(Identifier.UseCode.old)
                        .period(period3)
                        .system("System")
                        .type(ccType)
                        .value("value")
                        .build()
        );
        qualifications.add(
                Qualification.builder()
                        .period(period3)
                        .code(ccType)
                        .identifier(qidentifiers)
                        .build()
        );
        contactPoints.add(
                ContactPoint.builder()
                        .period(period2)
                        .rank(1)
                        .system(ContactPoint.SystemCode.email)
                        .use(ContactPoint.UseCode.home)
                        .value("pirker@spengergasse.at")
                        .build()
                 //new ContactPoint(ContactPoint.SystemCode.phone, "123454321", ContactPoint.UseCode.home, 1, period2)
                );


        List<String> givenNames = new ArrayList<>();
        givenNames.add("Simon");
        givenNames.add("2.Vorname");

        names.add(HumanName.builder()
                .family("Pirker")
                .given(givenNames)
                .period(Period.builder().start(LocalDateTime.now()).end(LocalDateTime.now()).build())
                .use(HumanName.UseCode.anonymous).build());

        address.add(Address.builder()
                        .city("Wien")
                        .country("Österreich")
                        .district("Wien")
                        .line("Spengergasse 20")
                        .postalcode("1050")
                        .period(period3)
                        .state("Wien")
                        .text("<div>.../</div>")
                        .type(Address.TypeCode.both)
                        .use(Address.UseCode.home)
                        .build()
                );
        photos.add(
                Attachment.builder()
                        .language("English")
                        .creation(LocalDate.of(2000, 02, 20))
                        .data("data")
                        .contentType("Photo")
                        .size(32)
                        .title("Title")
                        .url("C://")
                        .hash("NS893en")
                        .build()
        );
        return Practitioner.builder()
                .active(true)
                .birthDate(LocalDate.of(1999, 01, 01))
                .identifier(identifiers)
                .gender(Practitioner.GenderCode.male)
                .name(names)
                .telecom(contactPoints)
                .address(address)
                .photo(photos)
                .communication(communications)
                .qualification(qualifications)
                .build();
    }


    @Test
    @Transactional
    public void testDeleteOnePractitioner() {
        //Practitioner wird erstellt
        Practitioner pr = returnOnePractitioner();
        //In DB speichern
        Practitioner savedPr = practitionerRepository.save(pr);
        //aus DB laden
        Practitioner loadedPractitioner = practitionerRepository.findById(savedPr.getId()).get();
        //delete Patient
        practitionerRepository.delete(savedPr);
        assertEquals(practitionerRepository.findById(savedPr.getId()).isEmpty(), true);
    }

    @Test
    @Transactional // Alles oder nichts (alle DB-Befehle/ keine)
    public void testUpdateOnePractitioner() {
        //1. Erstellen einer mit Daten befüllten Practitioner-instanz
        Practitioner pr = returnOnePractitioner();

        //2. Instanz in die DB speichern
        Practitioner savedPr = practitionerRepository.save(pr);

        // 3. Gespeicherte Daten aus der DB lesen
        Practitioner loadedPractitioner = practitionerRepository.findById(savedPr.getId()).get();
        loadedPractitioner.setActive(false);
        Practitioner updatedP = practitionerRepository.save(loadedPractitioner);
        Practitioner updateloadedPractitioner = practitionerRepository.findById(updatedP.getId()).get();


        // 4. Vergleich des gespeicherten Objekts mit dem geladenen (Attribute zwischen updatesP & updateloadedPractitioner)

        // Alle einfachen Datentypen können mit Equals verglichen werden.
        // Assert prüft, ob die beiden gleich sind. Schlägt ein Assert fehl, ist der Test fehlgeschlagen/
        // Asserts sind die eigentlichen "Tests"


        // TODO: ALLE ATTRIBUTE ÄNDERN
        assertEquals(updatedP.getBirthDate(), updateloadedPractitioner.getBirthDate());
        assertEquals(updatedP.getGender(), updateloadedPractitioner.getGender());
        assertEquals(updatedP.getText(), updateloadedPractitioner.getText());
        assertEquals(updatedP.isActive(), updateloadedPractitioner.isActive());
        //Es sollen alle Attribute verglichen werden, auch die geerbten.

        // Collections werden mit CollectionUtils auf Gleichheit getestet.
        // Dabei werden die einzelnen Elemente verglichen, nicht ob die Collectionobjekte gleich sind.
        assertTrue(CollectionUtils.isEqualCollection(pr.getIdentifier(), updateloadedPractitioner.getIdentifier()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getName(), updateloadedPractitioner.getName()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getTelecom(), updateloadedPractitioner.getTelecom()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getAddress(), updateloadedPractitioner.getAddress()));

        assertTrue(CollectionUtils.isEqualCollection(pr.getCommunication(), updateloadedPractitioner.getCommunication()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getPhoto(), updateloadedPractitioner.getPhoto()));
        assertTrue(CollectionUtils.isEqualCollection(pr.getQualification(), updateloadedPractitioner.getQualification()));

        //Es sollen alle Collections getestet werden.
    }

}
