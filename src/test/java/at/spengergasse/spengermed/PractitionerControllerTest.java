package at.spengergasse.spengermed;


import at.spengergasse.spengermed.models.Practitioner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

//Macht aus der Klasse eine Testklass
@SpringBootTest
// Darüber werden Anfragen an den Controller geschickt und die Antworten ausgewertet.
@AutoConfigureMockMvc
public class PractitionerControllerTest {
    // Autowired: macht Instanz
    // MockMvC: simuliert den http Client
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    // Erster Test, um alle Practitioner unter der URL /api/practitioner mit GET abzufragen.
    // andExpectüberprüft, ob der zurückgegebene Status 200 (OK) ist.
    @Test
    public void getAllPractitioners() {
        try {
            mockMvc.perform(MockMvcRequestBuilders
                    .get("/api/practitioner"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isOk()); // isOk = Statuscode 200
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Ein einzelner Practitioner wird mit der id mit GET abgefragt.
    // Dabei muss der Patient mit der id in der DB existieren.
    // im import.sql muss dieser Patient somit eingefügt werden.
    // andExpect überprüft, ob der zurückgegebene Status 200 (OK) ist.
    @Test
    public void getAPractitioner() {
        try {
            mockMvc.perform(MockMvcRequestBuilders
                    .get("/api/practitioner/4df34"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAPractitionerNotFound() {
        try {
            mockMvc.perform(MockMvcRequestBuilders
                    .get("/api/practitioner/jhgjkjjkz"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isNotFound());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Es wird ein neuer Patient mit POST an den Controller geschickt und somit in der DB gespeichert.
    // Wir können die Methode aus PatientRepositoryTest, die uns eine Patienten Instanz erzeugt auch hierverwenden.
    // Der erwartete Rückgabecode ist "CREATED" Also 201.
    @Test
    public void postAPractitioner() {
        Practitioner practitioner = PractitionerRepositoryTest.returnOnePractitioner();
        String json = null;
        try {
            json = om.writeValueAsString(practitioner);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            mockMvc.perform(MockMvcRequestBuilders.post("/api/patient/")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json)).andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // PUT aktualisiert einen Patienten. Dieser muss somit bereits in der DB existieren
    // (über import.sql)//Die id im Patienten und die id in der URL sollten gesetzt sein und
    // müssen in der DB existieren.//Wir erwarten ein 200-OK für einen aktualisierten Patienten.
    // Kein 201 CREATED, sonst wäre der Patient neu angelegt worden.
    @Test
    public void putAPractitioner() {
        Practitioner practitioner = PractitionerRepositoryTest.returnOnePractitioner();
        practitioner.setId("7439re");
        String json = null;
        try {
            json = om.writeValueAsString(practitioner);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            mockMvc.perform(MockMvcRequestBuilders.put("/api/practitioner/4df34")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Der Patient wird geöscht.
    // Die id muss es bereits geben.
    // Erwartete Antwort ist 200 (OK)
    @Test
    public void deleteAPractitioner() {
        try {
            mockMvc.perform(MockMvcRequestBuilders.delete("/api/practitioner/12l2"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}