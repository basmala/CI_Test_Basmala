INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('1', 'Content 1');
INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('22', 'Content 22');
INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('55', 'Content 55');

INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birthdate`,`p_deceaseddatetime`, `p_deceasedboolean`, `p_gender`) VALUES ('asdf',1, '2000-01-01', '2010-01-01', 1, 'male');
INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birthdate`,`p_gender`, `p_deceasedboolean`) VALUES ('gjuerighirgh', 1, '2000-01-01','unknown', 1);
INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birthdate`,`p_gender`, `p_deceasedboolean`) VALUES ('7439re', 0, '2001-04-05','male', 0);
INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birthdate`,`p_gender`, `p_deceasedboolean`) VALUES ('frejifgreu89', 1, '2010-01-21','female', 1);
INSERT INTO `spengermed`.`hn_humanname` (`id`, `hn_family`,`pp_end`, `pp_start`, `hn_text`, `hn_use`, `hn_p_id`) VALUES ('n1','Mustermann', '2099-12-31', '2000-01-01', 'blabla', 'usual','7439re');
INSERT INTO `spengermed`.`a_address` (`id`, `a_city`, `a_country`,`a_district`, `pp_end`, `pp_start`, `a_postalcode`, `a_state`, `a_p_id`)VALUES ('addr1', 'Orasje', 'BiH', 'Orase', '2099-12-31 16:36:34.000000','2020-04-27 16:36:49.000000', '1111', 'BiH', '7439re');

INSERT INTO `spengermed`.`pr_practitioner` (`id`, `pr_active`, `pr_birthdate`,`pr_gender`) VALUES ('4df34', 1, '2000-01-01','male');
INSERT INTO `spengermed`.`pr_practitioner` (`id`, `pr_active`, `pr_birthdate`,`pr_gender`) VALUES ('12l2', 0, '2000-01-01','unknown');

INSERT INTO `spengermed`.`hn_humanname` (`id`, `hn_family`, `pp_end`, `pp_start`, `hn_text`, `hn_use`, `hn_pr_id`) VALUES ('2','Schmidt', '2020-12-25', '2001-09-14', 'blablabla', 'official','4df34');
INSERT INTO `spengermed`.`hn_humanname` (`id`, `hn_family`, `pp_end`, `pp_start`, `hn_text`, `hn_use`, `hn_pr_id`) VALUES ('3','Müller', '2099-12-31', '2003-11-09', 'blabla', 'old','12l2');
INSERT INTO `spengermed`.`a_address` (`id`, `a_city`, `a_country`,`a_district`, `pp_end`, `pp_start`, `a_postalcode`, `a_state`, `a_pr_id`)VALUES ('addr2', 'Orasje', 'BiH', 'Orase', '2099-12-31 16:36:34.000000','2020-04-27 16:36:49.000000', '1111', 'BiH', '12l2');
INSERT INTO `spengermed`.`a_address` (`id`, `a_city`, `a_country`,`a_district`, `pp_end`, `pp_start`, `a_postalcode`, `a_state`, `a_pr_id`)VALUES ('addr3', 'Orasje', 'BiH', 'Orase', '2099-12-31 16:36:34.000000','2020-04-27 16:36:49.000000', '1111', 'BiH', '4df34');

INSERT INTO `spengermed`.`q_qualification` (`id`, `pp_end`, `pp_start`, `q_pr_id`) VALUES ('1111','2020-12-25', '2010-12-25', '4df34');
INSERT INTO `spengermed`.`q_qualification` (`id`, `pp_end`, `pp_start`, `q_pr_id`) VALUES ('2222','2020-12-25', '2010-12-25', '12l2');
INSERT INTO `spengermed`.`at_attachment` (`id`, `at_content_type`, `at_creation`, `at_data`, `at_hash`, `at_language`, `at_size`, `at_title`, `at_url`, `at_pr_photo`) VALUES ('3457754','DemoData', '2012-01-21', 'Text123', '8733ijsdf', 'German',25 , 'title', 'http://4df34', '4df34');
INSERT INTO `spengermed`.`at_attachment` (`id`, `at_content_type`, `at_creation`, `at_data`, `at_hash`, `at_language`, `at_size`, `at_title`, `at_url`, `at_pr_photo`) VALUES ('54362','DemoData', '2012-01-21', 'Text321', '43657e', 'German',25 , 'title', 'http://12l2', '12l2');

INSERT INTO `spengermed`.`cc_codeableconcept` (`id`, `cc_text`, `cc_pr_id`) VALUES (1, 'h033e', '4df34');
INSERT INTO `spengermed`.`cc_codeableconcept` (`id`, `cc_text`, `cc_pr_id`) VALUES (2, 'dfj3u4', '12l2');

INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('0001','official', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV34', 'gjuerighirgh');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('0002','secondary', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system2', 'VV343', '7439re');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('0003','official', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV3476', 'frejifgreu89');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('0004','temp', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV64', 'asdf');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_pr_id`) VALUES ('0005','usual', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system2', 'VV343', '4df34');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_pr_id`) VALUES ('0006','official', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV3412', '12l2');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_q_id`) VALUES ('0007','official', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system2', 'VV87', '1111');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_q_id`) VALUES ('0008','temp', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV12', '2222');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_cc_fk`) VALUES ('0009','secondary', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system2', 'VV1', '1');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_cc_fk`) VALUES ('00010','usual', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV3409', '2');


INSERT INTO `spengermed`.`c_coding` (`id`, `c_code`, `c_display`, `c_system`, `c_user_selected`, `c_version`, `c_codeableconcept_fk`) VALUES (1, 'code987', 'cc876', 'system87687', 1, '8j', 1);
INSERT INTO `spengermed`.`c_coding` (`id`, `c_code`, `c_display`, `c_system`, `c_user_selected`, `c_version`, `c_codeableconcept_fk`) VALUES (2, 'code876', 'cci872982', 'system76567', 1, 'kk', 2);

INSERT INTO `spengermed`.`cp_contactpoint` (`id`, `pp_end`, `pp_start`, `cp_rank`, `cp_system`, `cp_use`, `cp_value`, `cp_pr_id`, `cp_p_id`) VALUES (1, '2020-08-12 09:45:00', '2014-06-06 12:00:00', 444, 'pager', 'temp', 'huzg', '12l2', 'asdf');

INSERT INTO `spengermed`.`cc_codeableconcept` (`id`, `cc_text`) VALUES (3, '34x2');
INSERT INTO `spengermed`.`cc_codeableconcept` (`id`, `cc_text`) VALUES (4, '1231');

INSERT INTO `spengermed`.`pc_procedure` (`id`, `pc_status`, `pc_performed_age`, `pc_cc_fk`) VALUES ('81hwhd', 'preparation', '2009-01-01', 3);
INSERT INTO `spengermed`.`pc_procedure` (`id`, `pc_status`, `pc_performed_range`, `pc_cc_fk`) VALUES ('hd78ed', 'completed', 45, 4);

INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_pc_id`) VALUES ('00011','secondary', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system2', 'VV1', '81hwhd');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_pc_id`) VALUES ('00012','usual', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV3409', 'hd78ed');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_cc_fk`) VALUES ('00013','secondary', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system2', 'VV1', '3');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_cc_fk`) VALUES ('00014','usual', '2089-01-01 12:20:00', '2016-01-01 05:00:00', 'system1', 'VV3409', '4');

INSERT INTO `spengermed`.`c_coding` (`id`, `c_code`, `c_display`, `c_system`, `c_user_selected`, `c_version`, `c_codeableconcept_fk`) VALUES (3, 'code987', 'cc876', 'system87687', 1, '8j', 3);
INSERT INTO `spengermed`.`c_coding` (`id`, `c_code`, `c_display`, `c_system`, `c_user_selected`, `c_version`, `c_codeableconcept_fk`) VALUES (4, 'code876', 'cci872982', 'system76567', 1, 'kk', 4);