package at.spengergasse.spengermed.repositories;

import at.spengergasse.spengermed.models.Practitioner;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PractitionerRepository extends PagingAndSortingRepository<Practitioner, String> {

}
