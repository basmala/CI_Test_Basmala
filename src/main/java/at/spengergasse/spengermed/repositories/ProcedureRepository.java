package at.spengergasse.spengermed.repositories;

import at.spengergasse.spengermed.models.Procedure;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProcedureRepository extends PagingAndSortingRepository<Procedure, String> {

}
