package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="pc_procedure")
public class Procedure extends DomainResource{

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="i_pc_id", referencedColumnName = "id")
    private List<Identifier> identifier;

    public enum StatusCode{
        preparation("preparation"),
        in_progress("in-progress"),
        not_done("not-done"),
        on_hold("on-hold"),
        stopped("stopped"),
        completed("completed"),
        entered_in_error("entered-in_error"),
        unknown("unknown");

        private String value;
        private StatusCode(String value) {
            this.value = value;
        }
        public String toString() {
            return this.value;
        }
    }

    @Enumerated(EnumType.STRING)
    @Column(name="pc_status")
    private Procedure.StatusCode status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="pc_cc_fk", referencedColumnName = "id")
    private CodeableConcept statusWhy;

    @Column(name="pc_performedAge")
    private LocalDate performedAge;

    @Column(name="pc_performedRange")
    private int performedRange;

}
