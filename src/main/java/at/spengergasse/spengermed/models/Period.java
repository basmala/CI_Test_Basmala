package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.time.LocalDateTime;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Period {

    @Column(name = "pp_start")
    private LocalDateTime start;
    @Column(name = "pp_end")
    private LocalDateTime end;
}
