package at.spengergasse.spengermed.models;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "at_attachment")
@Builder
public class Attachment extends Element {


    @Column(name = "at_contentType")
    private String contentType;

    @Column(name = "at_language")
    private String language;

    @Column(name="at_data")
    private String data;

    @Column(name="at_url")
    private String url;

    @Column(name="at_size")
    private int size;

    @Column(name="at_hash")
    private String hash;

    @Column(name="at_title")
    private String title;

    @Column(name="at_creation")
    private LocalDate creation;
}
