package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="q_qualification")
@Builder
public class Qualification extends BackboneElement{
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "i_q_id", referencedColumnName = "id")
    private List<Identifier> identifier;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="q_cc_id", referencedColumnName = "id")
    private CodeableConcept code;

    @Column(name="q_period")
    private Period period;
}
