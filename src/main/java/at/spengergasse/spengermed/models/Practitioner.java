package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="pr_practitioner")
public class Practitioner extends DomainResource{

    @Column(name="pr_active")
    private boolean active;

    public enum GenderCode{
        male, female, other, unknown
    }

    @Enumerated(EnumType.STRING)
    @Column(name="pr_gender")
    private Practitioner.GenderCode gender;

    @Column(name="pr_birthdate")
    private LocalDate birthDate;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="i_pr_id", referencedColumnName = "id")
    private List<Identifier> identifier;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="hn_pr_id", referencedColumnName = "id")
    private List<HumanName> name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="cp_pr_id", referencedColumnName = "id")
    private List<ContactPoint> telecom;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="a_pr_id", referencedColumnName = "id")
    private List<Address> address;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "at_pr_photo", referencedColumnName = "id")
    private List<Attachment> photo;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "q_pr_id", referencedColumnName = "id")
    private List<Qualification> qualification;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="cc_pr_id", referencedColumnName = "id")
    private List<CodeableConcept> communication;
}
