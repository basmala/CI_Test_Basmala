package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Greeting {
// in json format http zurückgeben, jpa erstellt db jedes mal
// über greetingrepository auf db zugreifen
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String content;

}
