package at.spengergasse.spengermed.controller;

import at.spengergasse.spengermed.models.Practitioner;
import at.spengergasse.spengermed.repositories.PractitionerRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RequestMapping(path = "/api/practitioner")
@RestController
@CrossOrigin
public class PractitionerController {

    @Autowired
    private PractitionerRepository practitionerRepository;

    @GetMapping
    public @ResponseBody Iterable<Practitioner> getAllPractitioners() {
        return practitionerRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Practitioner> getPractitioner(@PathVariable String id) {
        return practitionerRepository
                .findById(id)
                .map(practitioner -> ResponseEntity.ok().body(practitioner))
                .orElse(ResponseEntity.notFound().build());
    }

    // Create a new Practitioner
    @PostMapping()
    public ResponseEntity<Practitioner> createPractitioner(@Valid @RequestBody
                                                                  Practitioner practitioner) {
        practitioner.setId(null); // ensure to create new names
        var saved = practitionerRepository.save(practitioner);
        return ResponseEntity.created(URI.create("/api/practitioner/" +
                saved.getId())).body(saved);
    }

    // Update a Practitioner
    @PutMapping("/{id}")
    public ResponseEntity<Practitioner> updatePractitioner(
            @PathVariable(value = "id") String practitionerId, @Valid
    @RequestBody Practitioner practitionerDetails) {
        return practitionerRepository
                .findById(practitionerId)
                .map(
                        practitioner -> {
                            practitioner.setActive(practitionerDetails.isActive());
                            practitioner.setGender(practitionerDetails.getGender());
                            practitioner.setIdentifier(practitionerDetails.getIdentifier());
                            practitioner.setName(practitionerDetails.getName());
                            practitioner.setBirthDate(practitionerDetails.getBirthDate());
                            practitioner.setAddress(practitionerDetails.getAddress());
                            practitioner.setCommunication(practitionerDetails.getCommunication());
                            practitioner.setPhoto(practitionerDetails.getPhoto());
                            practitioner.setQualification(practitionerDetails.getQualification());
                            practitioner.setTelecom(practitionerDetails.getTelecom());

                            Practitioner updatePractitioner =
                                    practitionerRepository.save(practitioner);
                            return ResponseEntity.ok(updatePractitioner);
                        })
                .orElse(createPractitioner(practitionerDetails));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Practitioner> deletePractitioner(@PathVariable(value =
            "id") String practitionerId) {
        return practitionerRepository
                .findById(practitionerId)
                .map(
                        practitioner -> {
                            practitionerRepository.delete(practitioner);
                            return ResponseEntity.ok().<Practitioner>build();
                        })
                .orElse(ResponseEntity.notFound().build());}
}
