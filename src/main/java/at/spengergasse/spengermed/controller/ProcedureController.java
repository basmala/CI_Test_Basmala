package at.spengergasse.spengermed.controller;

import at.spengergasse.spengermed.models.Practitioner;
import at.spengergasse.spengermed.models.Procedure;
import at.spengergasse.spengermed.repositories.ProcedureRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RequestMapping(path = "/api/procedure")
@RestController
@CrossOrigin
public class ProcedureController {

    @Autowired
    private ProcedureRepository procedureRepository;

    @GetMapping
    public @ResponseBody Iterable<Procedure> getAllProcedures() {
        return procedureRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Procedure> getProcedure(@PathVariable String id) {
        return procedureRepository
                .findById(id)
                .map(procedure -> ResponseEntity.ok().body(procedure))
                .orElse(ResponseEntity.notFound().build());
    }

    // Create a new Procedure
    @PostMapping()
    public ResponseEntity<Procedure> createProcedure(@Valid @RequestBody
                                                                  Procedure procedure) {
        procedure.setId(null); // ensure to create new names
        var saved = procedureRepository.save(procedure);
        return ResponseEntity.created(URI.create("/api/procedure/" +
                saved.getId())).body(saved);
    }

    // Update a Procedure
    @PutMapping("/{id}")
    public ResponseEntity<Procedure> updateProcedure(
            @PathVariable(value = "id") String prodecureId, @Valid
    @RequestBody Procedure procedureDetails) {
        return procedureRepository
                .findById(prodecureId)
                .map(
                        procedure -> {
                            procedure.setIdentifier((procedureDetails.getIdentifier()));
                            procedure.setStatus(procedureDetails.getStatus());
                            procedure.setStatusWhy(procedureDetails.getStatusWhy());
                            procedure.setPerformedAge(procedureDetails.getPerformedAge());
                            procedure.setPerformedRange(procedureDetails.getPerformedRange());

                            Procedure updateProcedure =
                                    procedureRepository.save(procedure);
                            return ResponseEntity.ok(updateProcedure);
                        })
                .orElse(createProcedure(procedureDetails));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Procedure> deleteProcedure(@PathVariable(value =
            "id") String procedureId) {
        return procedureRepository
                .findById(procedureId)
                .map(
                        procedure -> {
                            procedureRepository.delete(procedure);
                            return ResponseEntity.ok().<Procedure>build();
                        })
                .orElse(ResponseEntity.notFound().build());}
}
